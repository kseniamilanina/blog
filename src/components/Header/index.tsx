import React from 'react';
import { NavLink } from 'react-router-dom';
import {routeMain as routeMainPage} from '../../pages/MainPage';
import {routeMain as routePosts} from '../../pages/Posts';

import './styles.scss';



const Header = () => {
  return (
    <header className="mainHeader">
      <h2 className='title'> Post Blog </h2>
      <nav>
        <NavLink to={routeMainPage()} activeClassName={'linkActive'}>
          Главная
        </NavLink>
        <NavLink to={routePosts()} activeClassName={'linkActive'}>
          Посты
        </NavLink>
      </nav>
    </header>
  );
}

export default Header;