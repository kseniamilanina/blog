import React from 'react';
import './styles.scss';


const Footer = () => {
  return (
    <footer className="mainFooter">
       <div className='work'>
          <p className='type'>Single Page Application</p>
          <p className='skills'>TYpeScript, JS, SASS, HTML, React, Router</p>
       </div>
       <div className='title'> Post Blog </div>   
      <div className='author'> Made by Ksenia Milanina</div>
    </footer>
  );
}

export default Footer;