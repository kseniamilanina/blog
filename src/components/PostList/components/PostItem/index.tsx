import React from "react";
import { NavLink } from "react-router-dom";
import { routeMain as routePostDetail} from "../../../../pages/PostsDetail";
import { IPostDetail } from "../../../../types/IPostDetail";
import './styles.scss';

interface IPostItemParams {
    item: IPostDetail; 
}
const PostItem:React.FC<IPostItemParams> = ({item}) => (
    <NavLink className="postItem"
    to = {routePostDetail(String(item.id))}
    >
        <div className="id">{item.id}</div>
        <div className="title">{item.title}</div>
    </NavLink>
)
export default PostItem;
