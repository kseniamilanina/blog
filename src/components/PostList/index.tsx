import React from "react";
import { IPostDetail } from "../../types/IPostDetail";
import PostItem from "./components/PostItem";
import './styles.scss';

interface IPostListParams {
    list: IPostDetail[];
}

const PostList: React.FC<IPostListParams> = ({list}) => (
    <div className="postList">
        {list.map((posts: IPostDetail) => (
            <PostItem key={posts.id} item={posts}/>
        ))}
    </div>

)

export default PostList;