import React from 'react';
import AppContent from '../AppContent';


const App = () => {
  return (
    <div className="App">
      <AppContent/>
    </div>
  );
}

export default App;
