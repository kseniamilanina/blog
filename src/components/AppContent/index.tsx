import React from "react";
import {Route, Switch, Redirect} from 'react-router-dom';
import MainPage, {routeMain as routeMainPage} from '../../pages/MainPage';
import Posts, {routeMain as routePosts} from "../../pages/Posts";
import PostDetail, {routeMain as routePostDetail} from "../../pages/PostsDetail";
import Header from "../Header";
import Footer from "../Footer";
import './styles.scss';
const AppContent = () => {
    return (
        <div className="mainWrapper">
            <Header/>
            <main>
                <Switch>
                <Route exact path={routeMainPage()} component={MainPage}/>
                <Route exact path={routePosts()} component={Posts}/>
                <Route exact path={routePostDetail()} component={PostDetail}/>
                <Redirect
                to = {{
                    pathname: routeMainPage()
                }}
                />
                </Switch>
            </main>
            <Footer/>

        </div>
    )
}
export default AppContent;