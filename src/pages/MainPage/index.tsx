import React, {useState, useEffect} from "react";
import routeMain from "./routes";
import { IPostDetail } from "../../types/IPostDetail";
import getPosts from "../../services/getPosts";
import PostList from "../../components/PostList";

import './styles.scss';

const MainPage = () => { 
    const [postList, setPostList] = useState<IPostDetail[]>([]);

    useEffect(() => {
        getPosts().then(response => {
            setPostList(response.data)
        })
    }, [])
    
    return(
        <section className="mainPage">
            {postList.length > 0 && <PostList list = {postList.slice(0,8)}/>}

        </section>
    )
    
}

export {routeMain};
export default MainPage;