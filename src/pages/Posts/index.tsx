import React, {useState, useEffect} from "react";
import { IPostDetail } from "../../types/IPostDetail";
import PostList from "../../components/PostList";
import getPosts from "../../services/getPosts";
import routeMain from "./routes";

import './styles.scss';

const Posts = () => { 

    const [postList, setPostList] = useState<IPostDetail[]>([]);

    useEffect(() => {
        getPosts().then(response => {
            setPostList(response.data)
        })
    }, [])
    
    return(
        <section className="mainPage">
            {postList.length > 0 && <PostList list = {postList}/>}

        </section>
    )
    
}

export {routeMain};
export default Posts;