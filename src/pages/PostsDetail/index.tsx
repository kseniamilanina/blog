import React, {useState, useEffect} from "react";
import { useParams } from "react-router-dom";
import getPosts from "../../services/getPosts";
import routeMain from "./routes";
import { ID } from "../../types/ID";
import { IPostDetail } from "../../types/IPostDetail";

import './styles.scss';
 
const PostDetail = () => {
    const {id} = useParams<ID>(); 
    const [post, setPost] = useState<IPostDetail | null>(null);

    useEffect(() => {
        getPosts().then(response => {
            const currentPost= response.data?.find(
                (item: IPostDetail)=> String(item.id) === id
            )
            setPost(currentPost);
        })
    }, [id])
    
    return(
        <section className="postDetailPage">
            {post? 
            (<div className="postWrapper">
                <div className="title">{post.title}</div>
                <div className="body">{post.body}</div>
            </div>
            )
            : <></>
        }
            
        </section>
    )
}
export {routeMain};
export default PostDetail;